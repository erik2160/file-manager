using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Avalonia.Controls;
using Avalonia.Interactivity;
using DynamicData;

namespace FileManager.Views;

public partial class WindowSelectToImport : Window
{
    private string[] _path = Directory.GetFiles("../../../Assets/Files");
    private ObservableCollection<string> _contentListBox = new();
    public WindowSelectToImport()
    {
        InitializeComponent();
        insertFilesOnListBox();
    }
    private void insertFilesOnListBox()
    {
        if (!hasFilesOnFolder())
        {
            VazioTextBlock.IsVisible = true;
        }
        else
        {
            foreach (var i in _path)
            {
                using (StreamReader reader = new StreamReader(i))
                {
                    _contentListBox.Add(Path.GetFileNameWithoutExtension(i));
                }
            }
            ImportFilesListBox.ItemsSource = _contentListBox.OrderBy(x => x);
        }
    }

    private bool hasFilesOnFolder()
    {
        return _path.Length switch
        {
            0 => false,
            _ => true
        };
    }

    private void ImportFilesListBox_OnSelectionChanged(object? sender, SelectionChangedEventArgs e)
    {
        ImportContentButton.IsEnabled = true;
    }

    private void ImportContentButton_OnClick(object? sender, RoutedEventArgs e)
    {
        var arquivo = ImportFilesListBox.SelectedItem.ToString();
        var conteudo = (File.ReadAllText($"../../../Assets/Files/{arquivo}.csv"));
        InsertImportedContentOnListBox(conteudo);
    }

    private void InsertImportedContentOnListBox(string conteudo)
    {
        MainWindow.ListaDePalavrasContent.Clear();
        string[] conteudoSeparado = conteudo.Split(",");
        
        MainWindow.ListaDePalavrasContent.AddRange(conteudoSeparado); // LINQ, TÁ ATUANDO COMO UM FOREACH
        
        PopUpImportWindow.Close();
    }
}