using System;
using System.IO;
using System.Security.AccessControl;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

namespace FileManager.Views;

public partial class WindowSelectToExport : Window
{
    private static string path = "../../../Assets/Files";
    private string fullPath = Path.GetFullPath(path);
    
    public WindowSelectToExport()
    {
        InitializeComponent();
    }

    private bool IsFieldEmpty()
    {
        if (string.IsNullOrEmpty(ArquivoNomeTextBox.Text))
        {
            return true;
        }

        return false;
    }

    private void ArquivoNomeTextBox_OnTextChanged(object? sender, TextChangedEventArgs e)
    {
        if (!IsFieldEmpty())
        {
            ExportButton.IsEnabled = true;
        }
        else
        {
            ExportButton.IsEnabled = false;
        }
    }
    //
    // private void ExportButton_OnClick(object? sender, RoutedEventArgs e)
    // {
    //     var nomeArquivo = ArquivoNomeTextBox.Text;
    //     ExportFile();
    // }
    //
    // private void ExportFile()
    // {
    //     string content = "teste";
    //
    //     // Cria ou abre o arquivo
    //     
    // }
}