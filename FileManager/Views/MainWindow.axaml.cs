using System;
using System.Collections.ObjectModel;
using Avalonia.Controls;
using Avalonia.Interactivity;

namespace FileManager.Views;

public partial class MainWindow : Window
{
    // private ObservableCollection<string> _listaDePalavras = [
    // "Abacaxi", "Cachorro", "Mesa", "Computador", "Livro", "Janela",
    // "Guitarra", "Chocolate", "Sol", "Felicidade", "Caminhar", "Apreciar",
    // "Escola", "Cachorro", "Gato", "Elefante", "Leão", "Tigre",
    // "Papagaio", "Girafa", "Zebra", "Macaco", "Cavalo", "Bola", "Cadeira",
    // "Armário", "Sofá", "Tapete", "Estante", "Luminária", "Quadro", "Espelho"
    // ];
    public static ObservableCollection<string> ListaDePalavrasContent = [];
    
    public MainWindow()
    {
        InitializeComponent();
        InsertInitialContentOnListbox();
    }

    private void ImportarButton_OnClick(object? sender, RoutedEventArgs e)
    {
        var secondWindow = new WindowSelectToImport();
        secondWindow.ShowDialog(this);
    }

    private void ExportarButton_OnClick(object? sender, RoutedEventArgs e)
    {
        var secondWindow = new WindowSelectToExport();
        secondWindow.ShowDialog(this);
    }

    private void InsertInitialContentOnListbox()
    {
        ListaDePalavrasContent.Add("Vazio!");
        UsersListBox.ItemsSource = ListaDePalavrasContent;
    }
}